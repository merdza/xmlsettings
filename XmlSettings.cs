﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace XmlSettings
{
    public class XmlSettings
    {
        public static string getSetting(string fileName, string setting)
        {
            try
            {
                string ret = "";
                XmlReader reader = new XmlTextReader(fileName);
                while (reader.Read())
                {
                    if (reader.NodeType == XmlNodeType.Element)
                    {
                        if (reader.Name == setting)
                        {
                            reader.Read();
                            if (reader.NodeType == XmlNodeType.Text)
                            {
                                ret = reader.Value;
                                reader.Close();
                                return ret;
                            }
                        }
                    }
                }
                reader.Close();
                return null;
            }
            catch (Exception ex)
            {
                throw new Exception("Nije uspelo čitanje iz settings fajla. " + ex.Message);
            }
        }

        //ova funkcija popunjava niz vrednosti, ako u settings fajlu postoji vise istih tagova
        public static void getArraySettings(string fileName, string setting, ref List<string> values)
        {
            try
            {
                XmlReader reader = new XmlTextReader(fileName);
                while (reader.Read())
                {
                    if (reader.NodeType == XmlNodeType.Element)
                    {
                        if (reader.Name == setting)
                        {
                            reader.Read();
                            if (reader.NodeType == XmlNodeType.Text)
                            {
                                values.Add(reader.Value);
                            }
                        }
                    }
                }
                reader.Close(); 
            }
            catch (Exception ex)
            {
                throw new Exception("Nije uspelo čitanje iz settings fajla. " + ex.Message);
            }
        }

        public static void setSetting(string fileName, string setting, string value)
        {
            try
            {
                XmlDocument doc = new XmlDocument();
                string xmlSettingsPath = fileName;
                doc.Load(xmlSettingsPath);

                XmlNode root = doc.DocumentElement;

                foreach (XmlNode node in root.ChildNodes)
                {
                    if (node.Name == setting)
                    {
                        node.InnerText = value;
                        break;
                    }
                }
                
                doc.Save(xmlSettingsPath);
            }
            catch (Exception ex)
            {
                throw new Exception("Nije uspelo upisivanje u settings fajl. " + ex.Message);
            }
        }
    }
}
